import 'package:flutter/material.dart';
import './transaction.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Expensify',
      theme: ThemeData(
        
        primarySwatch: Colors.blue,
        
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Expensify'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Transaction> transaction = [
     Transaction(id:"1", title:"Groceries", amount:500, date:DateTime.now()),
     Transaction(id:"2", title:"Clothes", amount:4000, date: DateTime.now())
  ];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      title:Text("Expensify")
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            
            width: double.infinity,
            child: Card(
              color: Colors.blue,
              child: Text("Chart"),
            )
          ),
          Card(
            child: Text("List of TX")
            )
        ],

      )
      ,
      );
  }
}
